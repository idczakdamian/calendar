'use strict';

/**
 * AJAX
 */

function ajax(options) {
	options = {
		type: options.type || "POST",
		url: options.url || "",
		onComplete: options.onComplete || function () { },
		onError: options.onError || function () { },
		onSuccess: options.onSuccess || function () { },
		dataType: options.dataType || "text"
	};

	var xml = new XMLHttpRequest();
	xml.open(options.type, options.url, true);

	xml.onreadystatechange = function () {
		if (xml.readyState == 4) {
			if (httpSuccess(xml)) {
				var returnData = (options.dataType == "xml") ? xml.responseXML : xml.responseText
				options.onSuccess(returnData);
			} else {
				options.onError();
			}
			options.onComplete();
			xml = null;
		}
	};

	xml.send();

	function httpSuccess(r) {
		try {
			return (r.status >= 200 && r.status < 300 || r.status == 304 || navigator.userAgent.indexOf("Safari") >= 0 && typeof r.status == "undefined")
		} catch (e) {
			return false;
		}
	}
}




/**
 * calendar
 */

(function(){

	let wrapperDay = document.getElementById('wrapper-day');
	let iconMonthUp = document.getElementById('icon-month-up');
	let iconMonthDown = document.getElementById('icon-month-down');

	let wrapperWeek = document.getElementById('wrapper-week');
	let eventsTitle = document.getElementById('events-title');

	let nameMonthLabel = document.querySelectorAll('[data-name-month]');
	let nameYearLabel = document.querySelectorAll('[data-number-year]');
	let nameFullYearLabel = document.querySelectorAll('[data-full-year]');
	let nameDayLabel = document.querySelectorAll('[data-name-day]');

	let today = new Date();

	let presetYear = today.getFullYear();
	let initialYear = presetYear - 100;
	let endedYear = presetYear + 100;

	let presentMonth = today.getMonth() + 1;

	let presentDay = today.getDate();

	let tempYear = presetYear;
	let tempMonth = presentMonth;


	// second value is leap month
	let quantityDayInMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];


	/**
	 *  set ONLY 'setLocaleLangCalendar' => pl-PL, en-EN, etc..
	 */
	
	let setLocaleLangCalendar = 'pl-PL';
	let setLocaleParametr = 0;

	switch (setLocaleLangCalendar){
		case 'pl-PL':
			setLocaleParametr = 0;
			break;

		case 'en-EN':
		    setLocaleParametr = 1;
			break;

		default:
			setLocaleParametr = 0;
			setLocaleLangCalendar = 'pl-PL';
	}


	const locale = [
		{ 
			lang: 'pl-PL',
			month: {
				longName: ['Styczeń','Luty', 'Marzec', 'Kwiecień', 'Maj', 'Czerwiec', 'Lipiec', 'Sierpień', 'Wrzesień', 'Październik', 'Listopad', 'Grudzień'],
				shortName: ['Sty','Lu','Mar','Kw','Maj','Cze','Lip','Sie','Wrz','Pa','Lis','Gru']
			},
			days: {
				longName: ['Poniedziałek', 'Wtorek', 'Środa', 'Czwartek', 'Piątek', 'Sobota', 'Niedziela'],
				shortName: ['Pon', 'Wt', 'Śr', 'Czw', 'Pt', 'Sob', 'Nd']
			},
			words: ['Wydarzenia']
		},
		{
			lang: 'en-EN',
			month: {
				longName:['January','February','March','April','May','June','July','August','September','October','November','December'],
				shortName:['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
			},
			days: {
				longName:['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday', 'Sunday'],
				shortName:['Mon','Tue','Wed','Thu','Fri','Sat', 'Sun']
			},
			words: ['Events']
		}
	];


	function daysOfWeekAndEventsTranslate(setLocaleLangCalendar){

		for(let i = 0; i < locale.length; i++){

			if(locale[i].lang === setLocaleLangCalendar){
				
				locale[i].days.shortName.forEach(el => {
					let div = document.createElement('div');
					div.className = 'calendar-box';
					div.innerText = el;
					wrapperWeek.appendChild(div);
				});

				eventsTitle.innerText = locale[i].words[0];

			}
		}
	};


	function checkLeapYears(year){
		if( ((year % 4 === 0) && (year % 100 !== 0)) || year % 400 === 0){
			return 1;
		}else{
			return -1;
		}
	};

	function checkDayName(dateStr, locale){
		let date = new Date(dateStr);
		return date.toLocaleDateString(locale, { weekday: 'long' });        
	};


	let yearStructure = [];
	function generateDayMonthYear(year, setLocaleParametr, setLocaleLangCalendar){

		if(checkLeapYears(year) === 1){
			quantityDayInMonth.splice(1, 1, 29);
		}else{
			quantityDayInMonth.splice(1, 1, 28);
		}
 
		for(let i = 1; i < 13; i++){

			// month / day/ year
			let getFirstDayName = checkDayName('0' + i + '/01' + '/' + year, setLocaleLangCalendar);
			let monthStructure = [];

			for(let j = 1; j < quantityDayInMonth[i - 1] + 1; j++){
			
				let getDaysName = checkDayName((i < 10 ? '0' : '') + i + '/' + (j < 10 ? '0' : '') + j + '/' + year, setLocaleLangCalendar);
				monthStructure.push({
					dayName: getDaysName,
					dayNumber: j
				});
			}

			yearStructure.push({
				year: year,
				monthName: locale[setLocaleParametr].month.longName[i - 1],
				monthNumber: i,
				dayInMonth: quantityDayInMonth[i - 1],
				nameFirstDayInMonth: getFirstDayName,
				monthDaysStructure: monthStructure
			});
		}

		return yearStructure;
	};

	function renderCalendarMonth(presentMonth, setLocaleParametr){
		if(wrapperDay){

			for(let i = 0; i < yearStructure.length; i ++){

				if(presentMonth === yearStructure[i].monthNumber){

					for(let a = 0; a < nameMonthLabel.length; a++){
						nameMonthLabel[a].innerText = yearStructure[i].monthName;
					}

					for(let a = 0; a < nameYearLabel.length; a++){
						nameYearLabel[a].innerText = yearStructure[i].year;
					}

					let firstDayNameFromStructure = yearStructure[i].nameFirstDayInMonth.toLocaleLowerCase();
					let checkNumberDayOfWeek = 0;
					for(let j = 0; j < locale[setLocaleParametr].days.longName.length; j++){
						if(locale[setLocaleParametr].days.longName[j].toLocaleLowerCase() === firstDayNameFromStructure){
							checkNumberDayOfWeek = j;
						}
					}

					let flagSlideDay = 0;
					for(let key in yearStructure[i].monthDaysStructure){
						if(flagSlideDay === 0){
							for(let j = 0; j < checkNumberDayOfWeek; j++){

								let div = document.createElement('div');
								div.className = 'calendar-box day-box day-from-last-month';
								wrapperDay.appendChild(div);
							}
							flagSlideDay = 1;
						}
						

						let div = document.createElement('div');
						div.innerText = yearStructure[i].monthDaysStructure[key].dayNumber;

						if(yearStructure[i].monthDaysStructure[key].dayNumber == presentDay && tempMonth === today.getMonth() + 1 && tempYear === today.getFullYear()){
							div.className = 'calendar-box day-box day-box-active-month day-active';
							div.setAttribute('data-attribute', ((yearStructure[i].monthDaysStructure[key].dayNumber < 10 ? '0' : '') + yearStructure[i].monthDaysStructure[key].dayNumber + '-' + (tempMonth < 10 ? '0' : '') + tempMonth + '-' + tempYear));
							div.setAttribute('data-day-name', yearStructure[i].monthDaysStructure[key].dayName.toLocaleLowerCase());
							
						}else{

							div.className = 'calendar-box day-box day-box-active-month';

							if((yearStructure[i].monthDaysStructure[key].dayNumber < presentDay && tempMonth === today.getMonth() + 1 && tempYear === today.getFullYear()) ||
							   (tempMonth < today.getMonth() + 1 && tempYear === today.getFullYear()) ||
							   (tempYear < today.getFullYear())){

								div.className = 'calendar-box day-box day-box-active-month day-box-active-month-previous-days';

							}							

							div.setAttribute('data-attribute', ((yearStructure[i].monthDaysStructure[key].dayNumber < 10 ? '0' : '') + yearStructure[i].monthDaysStructure[key].dayNumber + '-' + (tempMonth < 10 ? '0' : '') + tempMonth + '-' + tempYear));
							div.setAttribute('data-day-name', yearStructure[i].monthDaysStructure[key].dayName.toLocaleLowerCase());
						}

						wrapperDay.appendChild(div);
					}
				}
			}
		}
	};

	function firstLetterToUpperCase(string){
		return string.charAt(0).toUpperCase() + string.slice(1);
	};


	function setYearAndDayNameInText(childWrapperDay, i){
		for(let a = 0; a < nameFullYearLabel.length; a++){
			nameFullYearLabel[a].innerText = firstLetterToUpperCase(childWrapperDay[i].getAttribute('data-attribute'));
			nameFullYearLabel[a].value = firstLetterToUpperCase(childWrapperDay[i].getAttribute('data-attribute'));
		}

		for(let b = 0; b < nameDayLabel.length; b++){
			nameDayLabel[b].innerText = firstLetterToUpperCase(childWrapperDay[i].getAttribute('data-day-name'));
		}
	};


	function actionOnDayClick(){
		let childWrapperDay = wrapperDay.getElementsByClassName('day-box-active-month');
		let activeDay = wrapperDay.getElementsByClassName('day-active')[0];

		for(let i = 0; i < childWrapperDay.length; i++){

			if(activeDay){
				for(let a = 0; a < nameFullYearLabel.length; a++){
					nameFullYearLabel[a].innerText = firstLetterToUpperCase(activeDay.getAttribute('data-attribute'));
					nameFullYearLabel[a].value = firstLetterToUpperCase(activeDay.getAttribute('data-attribute'));
				}
		
				for(let b = 0; b < nameDayLabel.length; b++){
					nameDayLabel[b].innerText = firstLetterToUpperCase(activeDay.getAttribute('data-day-name'));
				}
			}
			

			childWrapperDay[i].addEventListener('click', function(){
				
				for(let j = 0; j < childWrapperDay.length; j++){
					childWrapperDay[j].classList.remove('day-active');
				}
				childWrapperDay[i].classList.add('day-active');

				setYearAndDayNameInText(childWrapperDay, i);
				
			});
		}
	};

	function changesMonths(){

		if(tempMonth < 1){
			tempMonth = 12;
			tempYear -= 1;
		}

		if(tempMonth > 12){
			tempMonth = 1;
			tempYear += 1;
		}

		while(wrapperDay.firstChild){
			wrapperDay.removeChild(wrapperDay.firstChild);
		}

		yearStructure = [];
		loadRefreshingFunctions();
			

		console.log(tempMonth + ' -->year ' + tempYear);
	};
	

	iconMonthDown.addEventListener('click', function(){
		tempMonth -= 1;
		changesMonths();
	});

	iconMonthUp.addEventListener('click', function(){
		tempMonth += 1;
		changesMonths();
	});



	daysOfWeekAndEventsTranslate(setLocaleLangCalendar);
	
	function loadRefreshingFunctions(){
		generateDayMonthYear(tempYear, setLocaleParametr, setLocaleLangCalendar);
		renderCalendarMonth(tempMonth, setLocaleParametr);
		actionOnDayClick();
	};

	loadRefreshingFunctions();
	
})();


/**
 * calendar modal
*/

(function(){

	function openModal(){

		let mask = document.getElementById('mask-modal');
		let modal = document.querySelectorAll('[modal-id]');
		let modalOpenButton = document.querySelectorAll('[data-modal-id]');

		for(let i = 0; i < modalOpenButton.length; i++){

			modalOpenButton[i].addEventListener('click', function(e){

				for(let j = 0; j < modal.length; j++){
					
					if(e.target.getAttribute('data-modal-id') === modal[j].getAttribute('modal-id')){
					
						if(!modal[j].classList.contains('wrapper-modal-display')){
							modal[j].classList.add('wrapper-modal-display');
							setTimeout(function(){
								modal[j].classList.add('wrapper-modal-open');
								mask.classList.add('mask-open-modal');
							}, 16);
						}else{
							modal[j].classList.remove('wrapper-modal-open');
							mask.classList.remove('mask-open-modal');
							setTimeout(function(){
								modal[j].classList.remove('wrapper-modal-display')
							}, 100);
						}
					}
				}
			});
		}
	}

	function closeModal(){
		let mask = document.getElementById('mask-modal');
		let modal = document.querySelectorAll('[modal-id]');
		let modalCloseButton = document.querySelectorAll('[modal-close]');

		for(let i = 0; i < modalCloseButton.length; i++){

			modalCloseButton[i].addEventListener('click', function(){

				for(let j = 0; j < modal.length; j++){

					modal[j].classList.remove('wrapper-modal-open');
					mask.classList.remove('mask-open-modal');
					setTimeout(function(){
						modal[j].classList.remove('wrapper-modal-display')
					}, 100);
				}
			});
		}
	}
	

	openModal();
	closeModal();
	
})();



/**
 * calendar events
*/

(function(){

	let nameFullYearLabel = document.querySelector('[data-full-year]');
	let loadEvents = document.getElementById('load-events');
	let childWrapperDay = document.getElementsByClassName('day-box-active-month');
	let iconMonthUp = document.getElementById('icon-month-up');
	let iconMonthDown = document.getElementById('icon-month-down');

	let notificationEvents = document.createElement('span');
	notificationEvents.className = 'day-event';

	function getEventsDayAjax(dateParam){
		ajax({
			type: "POST",
			url: "calendar/events/" + dateParam,
			dataType: "json",
			onError: function (msg) {
				console.warn(msg)
			},
			onSuccess: function (msg) {
				loadEvents.innerHTML = msg;
				deleteEventsAjax();
			}
		});
	};

	function deleteEventsAjax(){
		let deleteButton = document.querySelectorAll('[data-delete-events]');
		for(let i = 0; i < deleteButton.length; i++){
			deleteButton[i].addEventListener('click', function(event){
				ajax({
					type: "POST",
					url: "calendar/delete-events/" + event.target.getAttribute('data-delete-events'),
					dataType: "json",
					onError: function (msg) {
						console.warn(msg)
					},
					onSuccess: function (msg) {
						let wrapperList = document.getElementById('event-list-ul');
						let list = document.querySelectorAll('[data-delete-li]');
						for(let j = 0; j < list.length; j++){
							if(event.target.getAttribute('data-delete-events') === list[j].getAttribute('data-delete-li')){
								wrapperList.removeChild(list[j]);
							}
						}
					}
				});
			});
		}
	}

	function loopOnDays(childWrapperDay){
		for(let i = 0; i < childWrapperDay.length; i++){
			childWrapperDay[i].addEventListener('click', function(event){
				getEventsDayAjax(event.target.getAttribute('data-attribute'));
			});
		}
	};

	window.addEventListener('load', function(){
		getEventsDayAjax(nameFullYearLabel.value);
	});

	loopOnDays(childWrapperDay);

	[iconMonthUp, iconMonthDown].forEach(function(changeMonthParam){
		changeMonthParam.addEventListener('click', function(){
			loopOnDays(childWrapperDay);
		});
	});



	

})();