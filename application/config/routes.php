<?php
defined('BASEPATH') OR exit('No direct script access allowed');



$route = [
    'translate_uri_dashes' => false,
    'default_controller'   => 'calendar',
    '404_override'         => '',

    'calendar/events/([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))' => 'calendar/events/$1',
    'calendar/add-events' => 'calendar/addEvents',
    'calendar/delete-events/(:num)' => 'calendar/deleteEvents/$1'
];