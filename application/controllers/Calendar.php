<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Calendar extends CI_Controller {

	public function __construct(){
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
    }

    private $data = [];
    private function templateLoader($template_name){

        $this->load->view('calendar/meta', $this->data);
        $this->load->view('calendar/'. $template_name, $this->data);
        $this->load->view('calendar/footer', $this->data);
    }

    private function redirect($url, $statusCode = 301){
        
        header('Location: ' . $url, true, $statusCode);
        die();

    }

    public function index(){

        $this->templateLoader('index');

    }

    public function events($date){

        $dataAjax = [];

        $newFormatDate = date("Y-m-d", strtotime($date));

        $events = $this->db->query('SELECT * FROM events WHERE date = ?', [$newFormatDate]);
        $this->dataAjax['events'] = $events->result();
        $this->load->view('calendar/events', $this->dataAjax);
    }


    public function addEvents(){

        if($post = $this->input->post()){

            $title = htmlspecialchars($this->input->post('events-title-add'));
            $desc = htmlspecialchars($this->input->post('events-description-add'));
            $date = htmlspecialchars($this->input->post('events-date-add'));
            $newFormatDate = date("Y-m-d", strtotime($date));

            if(strlen($title) > 0 && preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $newFormatDate)){

                if(strlen($title > 40)){
                    $title = substr($title, 0, 40);
                }
                if(strlen($desc) > 0){
                
                    if(strlen($desc) > 0){
                        $desc = substr($desc, 0, 200);
                    }
                }else{
                    $desc = '';
                }
                $this->db->query('INSERT INTO events SET title = ?, description = ?, active = ?, date = ?', [$title, $desc, 1, $newFormatDate]);
                $this->redirect('/');
            }
        }else{
            $this->redirect('/');
        }
    }

    public function deleteEvents($id){
        $this->db->query('DELETE FROM events WHERE id = ?', $id);
    }







}
