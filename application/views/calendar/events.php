<ul id="event-list-ul" class="event-list-ul">
    <?php
    foreach($events as $event){
        ?>
        <li data-delete-li="<?php echo $event->id;?>" class="event-list-li">
            <?php echo $event->title;?>
            <p class="paragraph-events">
                <?php echo $event->description;?>
            </p>
            <div class="wrapper-button-events-list">
                <button data-delete-events="<?php echo $event->id;?>" class="icon-trash-empty event-list-button"></button>
            </div>
        </li>
        <?php
    }
    ?>
</ul>