<body class="body">

	<div id="mask-modal" class="mask-modal"></div>
	<div id="calendar" class="wrapper-calendar">
		<div id="calendar-panel" class="calendar-panel">
			<ul id="navigate-panel-ul" class="navigate-panel-ul">
				<li class="navigate-panel-li">
					<span class="visuallyhidden">Dodaj wydarzenie</span>
					<span data-modal-id="open-event-modal" class="icon-plus icon-navigate-panel" aria-hidden="true" title="dodaj wydarzenie"></span>
				</li>
			</ul>
			<div id="calendar-content" class="wrapper-content-panel-calendar">
				<div class="panel-date">
					<span class="panel-date-title panel-date-year"><span data-name-month>Czerwiec</span><span class="month-year" data-full-year>30-06-2018</span></span>
					<span class="panel-date-title panel-date-week"><span data-name-day></span></span>
				</div>
				<div class="panel-events">
					<h2 id="events-title" class="event-title">Wydarzenia</h2>
					<div id="load-events"></div>
				</div>
			</div>
		</div>
		<div id="calendar-body" class="calendar-body">
			<div class="wrapper-month">
				<span class="month-name"><span data-name-month>Czerwiec</span> <span data-number-year>2018</span></span>
				<div class="wrapper-icon-month">
					<span id="icon-month-up" aria-hidden="true" class="icon-angle-up icon-month"></span>
					<span id="icon-month-down" aria-hidden="true" class="icon-angle-down icon-month"></span>
				</div>
			</div>
			<div id="wrapper-week" class="wrapper-week">
				
			</div>
			<div id="wrapper-day" class="wrapper-day">
				
			</div>
		</div>
	</div>
	<div modal-id="open-event-modal" class="wrapper-modal">
		<form id="add-event-form" action="calendar/add-events" method="POST">
			<button type="button" class="modal-icon-cancel icon-cancel" aria-hidden="true" title="Zamknij modal" modal-close>
				<span class="visuallyhidden">Zamknij modal</span>
			</button>
			<h2 class="modal-title">Dodaj wydarzenie</h2>
			<div class="wrapper-content-modal">
				<div class="wrapper-inputs-modal">
					<label class="modal-label" for="modal-event-name-add">Nazwa wydarzenia</label>
					<input id="modal-event-name-add" class="modal-input" name="events-title-add" required maxlength="40">
				</div>
				<div class="wrapper-inputs-modal">
					<label class="modal-label" for="modal-event-desc-add">Opis *</label>
					<textarea id="modal-event-desc-add" class="modal-area" name="events-description-add" maxlength="200"></textarea>
				</div>

				<div class="wrapper-footer-modal">
					<input data-full-year name="events-date-add" type="hidden">
					<button class="modal-btn modal-btn-close" modal-close>Anuluj</button>
					<button id="modal-add-events" class="modal-btn modal-btn-add">Dodaj</button>
				</div>
			</div>
		</form>
	</div>