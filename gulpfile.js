const gulp = require('gulp');
const browserSync = require('browser-sync');
const sass = require('gulp-sass');
const sourceMaps = require('gulp-sourcemaps');
const autoPrefixer = require('gulp-autoprefixer');
const cleanCSS = require('gulp-clean-css');
const uglify = require('gulp-uglifyes');
const imagemin = require('gulp-imagemin');
const changed = require('gulp-changed');


const config = {
	server: 'http://calendar.local',
	dist: 'dist/',
	follow_html: 'application/views/**/*.php',
	follow_scss: 'public/scss/**/*.scss',
	sccs_out: 'public/css/',
	css_in: 'public/css/**/*.css',
	css_out: 'dist/css',
	js_in: 'public/js/**/*.js',
	js_out: 'dist/js',
	img_in: 'public/img/**/*.{jpg,jpeg,png,gif}',
	img_out: 'dist/img'
};

gulp.task('reload', function(){
	browserSync.reload();
});


gulp.task('serve', ['sass'], function(){
	
	browserSync.init({
       proxy: config.server
    });

	gulp.watch(config.follow_html, ['reload']);
	gulp.watch(config.follow_scss, ['sass']);

});

gulp.task('sass', function(){
	return gulp.src(config.follow_scss)
		.pipe(sourceMaps.init())
		.pipe(sass().on('error', sass.logError))
		.pipe(autoPrefixer({
			browsers:['last 6 versions']
		}))
		.pipe(sourceMaps.write())
		.pipe(gulp.dest(config.sccs_out))
		.pipe(browserSync.stream());
});

gulp.task('minifycss', function(){
	return gulp.src(config.css_in)
		.pipe(cleanCSS())
		.pipe(gulp.dest(config.css_out));
});

gulp.task('minifyjs', function(){
	return gulp.src(config.js_in)
		.pipe(uglify())
		.pipe(gulp.dest(config.js_out));
});

gulp.task('minifyimg', function() {
  return gulp.src(config.img_in)
    .pipe(changed(config.img_out))
    .pipe(imagemin())
    .pipe(gulp.dest(config.img_out));
});


gulp.task('default', ['serve']);